﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Serialization.Config;

namespace Data.Protocol.UnitTest
{
    [TestClass]
    public class SerializationTests
    {
        [TestMethod]
        public void TextMessageTest()
        {
            SerializationService service = SerializationProvider.Instance.SerializationService;

            TextMessage message = new TextMessage
            {
                TimeStamp = new DateTime(2015, 12, 10, 12, 0, 0),
                Text = "Тест сериализации",
                Comment = "Сохраняется во второй версии сериализатора."
            };

            //Есть метод, который берёт версию из объекта. Для теста используем этот метод.
            byte[] serializedVer1 = service.Serialize(message);

            message.SetVersionForTest(2);
            byte[] serializedVer2 = service.Serialize(message);

            TextMessage deserializedVer1 = service.Deserialize<TextMessage>(serializedVer1);
            TextMessage deserializedVer2 = service.Deserialize<TextMessage>(serializedVer2);


            bool successVer1 = message.TimeStamp == deserializedVer1.TimeStamp &&
                          message.Text.Equals(deserializedVer1.Text, StringComparison.Ordinal);


            bool successVer2 = message.TimeStamp == deserializedVer2.TimeStamp &&
                          message.Text.Equals(deserializedVer2.Text, StringComparison.Ordinal) &&
                          message.Comment.Equals(deserializedVer2.Comment, StringComparison.Ordinal);

            Assert.IsTrue(successVer1);
            Assert.IsTrue(successVer2);
        }
    }
}
