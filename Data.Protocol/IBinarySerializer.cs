using System.IO;

namespace Data.Protocol
{
    public interface IBinaryDeserializer<TData> : IVersionSupport
    {
        /// <summary>
        /// ��������������� ������.
        /// </summary>
        /// <param name="reader">�������� �����.</param>
        TData Deserialize(BinaryReader reader);

        /// <summary>
        /// ��������������� ������.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        TData Deserialize(byte[] bytes);
    }

    public interface IBinarySerializer<TData> : IVersionSupport
    {
        /// <summary>
        /// ������������� ������ � �������� �������������.
        /// </summary>
        /// <param name="data">������, ������� ����� �������������.</param>
        /// <param name="writer">������� �����.</param>
        /// <returns>���������� ���� ����� ������������.</returns>
        int Serialize(TData data, BinaryWriter writer);
    }
}