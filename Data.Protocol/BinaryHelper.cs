﻿using System;
using System.IO;
using System.Text;

namespace Data.Protocol
{
    public static class BinaryHelper
    {
        public static int Write(DateTime dateTime, BinaryWriter writer)
        {
            long beforeWrite = writer.BaseStream.Position;
            writer.Write(dateTime.Ticks);
            return (int)(writer.BaseStream.Position - beforeWrite);
        }

        public static DateTime ReadDateTime(BinaryReader reader)
        {
            long ticks = reader.ReadInt64();
            return new DateTime(ticks);
        }

        public static byte[] ToBytes(string message, Encoding encoding)
        {
            byte[] result = encoding.GetBytes(message);
            return result;
        }

        public static byte[] ToBytes(string message)
        {
            return ToBytes(message, Encoding.UTF8);
        }
    }
}