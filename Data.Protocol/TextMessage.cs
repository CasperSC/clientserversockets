﻿using System;

namespace Data.Protocol
{
    public class TextMessage : IVersionSupport, IChangeFormat
    {
        public const ushort FormatVersion = 1;

        protected ushort _version = FormatVersion;

        public virtual ushort Version
        {
            get { return _version; }
        }

        public void SetVersionForTest(ushort version)
        {
            _version = version;
        }

        public DateTime TimeStamp { get; set; }

        public string Text { get; set; }

        public string Comment { get; set; } //Добавлено позже, когда уже много сохранённых, сериализованных данных без этого свойства
    }

    public interface IChangeFormat
    {
        void SetVersionForTest(ushort version);
    }
}