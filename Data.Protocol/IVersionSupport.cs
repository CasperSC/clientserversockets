namespace Data.Protocol
{
    public interface IVersionSupport
    {
        ushort Version { get; }
    }
}