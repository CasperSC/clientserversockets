using System.Collections.Generic;

namespace Data.Protocol.Serialization
{
    public class DeserializersContainer
    {
        private readonly Dictionary<ushort, IVersionSupport> _deserializers;

        public DeserializersContainer()
        {
            _deserializers = new Dictionary<ushort, IVersionSupport>();
        }

        /// <summary>
        /// Зарегистрировать десериализатор. Нельзя зарегистрировать более одного десериализатора с
        /// однинаковой версией.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="deserializerVersion"></param>
        /// <param name="deserializer"></param>
        public void Register<T>(ushort deserializerVersion, IBinaryDeserializer<T> deserializer)
        {
            _deserializers.Add(deserializerVersion, deserializer);
        }

        public IBinaryDeserializer<T> GetDeserializer<T>(ushort version)
        {
            return (IBinaryDeserializer<T>)_deserializers[version];
        }
    }

    public class SerializersContainer
    {
        private readonly Dictionary<ushort, IVersionSupport> _serializers;

        public SerializersContainer()
        {
            _serializers = new Dictionary<ushort, IVersionSupport>();
        }

        /// <summary>
        /// Зарегистрировать сериализатор. Нельзя зарегистрировать более одного сериализатора с
        /// однинаковой версией.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializerVersion"></param>
        /// <param name="serializer"></param>
        public void Register<T>(ushort serializerVersion, IBinarySerializer<T> serializer)
        {
            _serializers.Add(serializerVersion, serializer);
        }

        public IBinarySerializer<T> GetSerializer<T>(ushort version)
        {
            return (IBinarySerializer<T>)_serializers[version];
        }
    }
}