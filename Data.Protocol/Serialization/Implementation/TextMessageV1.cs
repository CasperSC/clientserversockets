using System.IO;
using System.Text;

namespace Data.Protocol.Serialization.Implementation
{
    public class TextMessageV1 : IBinaryDeserializer<TextMessage>, IBinarySerializer<TextMessage>
    {
        public const ushort FormatVersion = 1;

        public virtual ushort Version { get { return FormatVersion; } }

        /// <summary>
        /// ������������� ������ � �������� �������������.
        /// </summary>
        /// <param name="data">������, ������� ����� �������������.</param>
        /// <param name="writer">������� �����.</param>
        /// <returns>���������� ���� ����� ������������.</returns>
        public virtual int Serialize(TextMessage data, BinaryWriter writer)
        {
            long startPosition = writer.BaseStream.Position;

            writer.Write(Version);
            BinaryHelper.Write(data.TimeStamp, writer);
            byte[] message = BinaryHelper.ToBytes(data.Text);
            writer.Write(message.Length);
            writer.Write(message);

            int packageLength = (int)(writer.BaseStream.Position - startPosition);
            return packageLength;
        }

        /// <summary>
        /// ��������������� ������.
        /// </summary>
        /// <param name="reader">�������� �����.</param>
        public TextMessage Deserialize(BinaryReader reader)
        {
            return DeserializeInternal(reader);
        }

        /// <summary>
        /// ��������������� ������.
        /// </summary>
        public TextMessage Deserialize(byte[] bytes)
        {
            TextMessage message;
            using (BinaryReader reader = new BinaryReader(new MemoryStream(bytes)))
            {
                message = DeserializeInternal(reader);
            }

            return message;
        }

        /// <summary>
        /// ��������������� ������.
        /// </summary>
        /// <param name="reader">�������� �����.</param>
        protected virtual TextMessage DeserializeInternal(BinaryReader reader)
        {
            var message = new TextMessage();
            message.TimeStamp = BinaryHelper.ReadDateTime(reader);
            int length = reader.ReadInt32();
            message.Text = Encoding.UTF8.GetString(reader.ReadBytes(length));

            return message;
        }
    }
}