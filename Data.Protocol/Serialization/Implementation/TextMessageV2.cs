using System.IO;
using System.Text;

namespace Data.Protocol.Serialization.Implementation
{
    public class TextMessageV2 : TextMessageV1
    {
        new public const ushort FormatVersion = 2;

        public override ushort Version { get { return FormatVersion; } }

        /// <summary>
        /// ������������� ������ � �������� �������������.
        /// </summary>
        /// <param name="data">������, ������� ����� �������������.</param>
        /// <param name="writer">������� �����.</param>
        /// <returns>���������� ���� ����� ������������.</returns>
        public override int Serialize(TextMessage data, BinaryWriter writer)
        {
            int length = base.Serialize(data, writer);
            long startPosition = writer.BaseStream.Position;

            byte[] commentBytes = BinaryHelper.ToBytes(data.Comment);
            writer.Write(commentBytes.Length);
            writer.Write(commentBytes);
            
            int packageLength = (int)(writer.BaseStream.Position - startPosition) + length;
            return packageLength;
        }

        /// <summary>
        /// ��������������� ������.
        /// </summary>
        /// <param name="reader">�������� �����.</param>
        protected override TextMessage DeserializeInternal(BinaryReader reader)
        {
            var message = base.DeserializeInternal(reader);
            int length = reader.ReadInt32();
            message.Comment = Encoding.UTF8.GetString(reader.ReadBytes(length));
            return message;
        }
    }
}