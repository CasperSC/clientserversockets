﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Data.Protocol;
using Serialization.Config;

namespace ConsoleClient
{
    class Program
    {
        private static Program _program;

        static void Main(string[] args)
        {
            _program = new Program();
            _program.Run();
        }

        private SerializationService _serializationService;

        /// <summary>
        /// Приложение запросило текст для отправки на сервер.
        /// </summary>
        public Func<string> RequestText;

        /// <summary>
        /// Данные для вывода обновлены.
        /// </summary>
        public event Action<string> OutputUpdated;

        protected virtual string OnRequestText()
        {
            var handler = RequestText;
            if (handler != null)
            {
                return handler();
            }

            return string.Empty;
        }

        protected virtual void OnOutputUpdated(string message, params object[] args)
        {
            var handler = OutputUpdated;
            if (handler != null)
            {
                handler(string.Format(message, args));
            }
        }

        protected virtual void OnOutputUpdated(string message)
        {
            var handler = OutputUpdated;
            if (handler != null)
            {
                handler(message);
            }
        }

        private void Run()
        {
            Console.Title = "Клиент";

            _serializationService = SerializationProvider.Instance.SerializationService;

            RequestText += Program_RequestText;
            OutputUpdated += Program_OutputUpdated;
            try
            {
                SendMessageFromSocket(11000);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadKey();
            }
        }

        private void Program_OutputUpdated(string message)
        {
            Console.WriteLine(message);
        }

        private string Program_RequestText()
        {
            Console.Write("Введите сообщение: ");
            string text = Console.ReadLine();
            return text;
        }

        private void SendMessageFromSocket(int port)
        {
            // Буфер для входящих данных
            byte[] bytes = new byte[1024];

            // Соединяемся с удаленным устройством

            // Устанавливаем удаленную точку для сокета
            IPHostEntry ipHost = Dns.GetHostEntry("127.0.0.1");
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);

            Socket sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            // Соединяем сокет с удаленной точкой
            sender.Connect(ipEndPoint);

            string text = OnRequestText();

            var message = new TextMessage
            {
                Text = text,
                TimeStamp = DateTime.Now
            };

            OnOutputUpdated("Сокет соединяется с {0} ", sender.RemoteEndPoint.ToString());

            byte[] msg = _serializationService.Serialize(message);

            // Отправляем данные через сокет
            int bytesSent = sender.Send(msg);

            // Получаем ответ от сервера
            int bytesRec = sender.Receive(bytes);

            OnOutputUpdated("\nОтвет от сервера: {0}\n\n", Encoding.UTF8.GetString(bytes, 0, bytesRec));

            // Освобождаем сокет
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
        }
    }
}
