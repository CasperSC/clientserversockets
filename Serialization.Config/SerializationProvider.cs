﻿using Data.Protocol;
using Data.Protocol.Serialization;
using Data.Protocol.Serialization.Implementation;

namespace Serialization.Config
{
    public sealed class SerializationProvider
    {
        public static SerializationProvider Instance { get; private set; }

        static SerializationProvider()
        {
            Instance = new SerializationProvider();
        }

        private SerializationProvider()
        {
            var service = new SerializationService();
            SerializationService = service;

            var textMessageV1 = new TextMessageV1();
            var textMessageV2 = new TextMessageV2();

            var deserializers = new DeserializersContainer();
            deserializers.Register(textMessageV1.Version, textMessageV1);
            deserializers.Register(textMessageV2.Version, textMessageV2);

            var serializers = new SerializersContainer();
            serializers.Register(textMessageV1.Version, textMessageV1);
            serializers.Register(textMessageV2.Version, textMessageV2);

            service.Register<TextMessage>(deserializers);
            service.Register<TextMessage>(serializers);
        }

        public SerializationService SerializationService { get; set; }
    }
}
