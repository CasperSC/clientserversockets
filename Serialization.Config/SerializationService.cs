﻿using System;
using System.Collections.Generic;
using System.IO;
using Data.Protocol;
using Data.Protocol.Serialization;

namespace Serialization.Config
{
    /// <summary>
    /// Поставщик десериализаторов для некоторых типов данных.
    /// </summary>
    public class SerializationService
    {
        /// <summary>
        /// <see cref="Type"/>  - тип данных, для которого нужно получить десериализатор.
        /// <see cref="DeserializersContainer"/> - контейнер десериализаторов для некоторого типа данных.
        /// </summary>
        private readonly Dictionary<Type, DeserializersContainer> _deserializers;

        /// <summary>
        /// <see cref="Type"/>  - тип данных, для которого нужно получить сериализатор.
        /// <see cref="SerializersContainer"/> - контейнер сериализаторов для некоторого типа данных.
        /// </summary>
        private readonly Dictionary<Type, SerializersContainer> _serializers;

        internal SerializationService()
        {
            _deserializers = new Dictionary<Type, DeserializersContainer>();
            _serializers = new Dictionary<Type, SerializersContainer>();
        }

        public void Register<T>(DeserializersContainer container)
            where T : IVersionSupport
        {
            _deserializers.Add(typeof(T), container);
        }

        public void Register<T>(SerializersContainer container)
            where T : IVersionSupport
        {
            _serializers.Add(typeof(T), container);
        }

        public IBinaryDeserializer<T> GetDeserializer<T>(ushort version)
            where T : IVersionSupport
        {
            return GetDeserializerInternal<T>(version);
        }

        public T Deserialize<T>(byte[] bytes)
            where T : IVersionSupport
        {
            T data;
            using (var reader = new BinaryReader(new MemoryStream(bytes)))
            {
                ushort version = reader.ReadUInt16();
                IBinaryDeserializer<T> deserializer = GetDeserializerInternal<T>(version);
                data = deserializer.Deserialize(reader);
            }

            return data;
        }

        public byte[] Serialize<T>(T data)
          where T : IVersionSupport
        {
            byte[] buffer = new byte[8096];
            using (var ms = new MemoryStream(buffer))
            using (var writer = new BinaryWriter(ms))
            {
                IBinarySerializer<T> serializer = GetSerializerInternal<T>(data.Version);
                int packageLength = serializer.Serialize(data, writer);
                byte[] result = new byte[packageLength];
                Array.Copy(buffer, result, packageLength);
                return result;
            }
        }

        private IBinaryDeserializer<T> GetDeserializerInternal<T>(ushort version)
        {
            return _deserializers[typeof(T)].GetDeserializer<T>(version);
        }

        private IBinarySerializer<T> GetSerializerInternal<T>(ushort version)
        {
            return _serializers[typeof(T)].GetSerializer<T>(version);
        }
    }
}