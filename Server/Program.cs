﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Net;
using Data.Protocol;
using Serialization.Config;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Сервер";

            SerializationService service = SerializationProvider.Instance.SerializationService;

            // Устанавливаем для сокета локальную конечную точку
            IPHostEntry ipHost = Dns.GetHostEntry("127.0.0.1");
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11000);

            // Создаем сокет Tcp/Ip
            Socket listener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            // Назначаем сокет локальной конечной точке и слушаем входящие сокеты
            try
            {
                listener.Bind(ipEndPoint);
                listener.Listen(10);

                // Начинаем слушать соединения
                while (true)
                {
                    Console.WriteLine("Ожидаем соединение через порт {0}", ipEndPoint);

                    // Программа приостанавливается, ожидая входящее соединение
                    Socket handler = listener.Accept();

                    // Мы дождались клиента, пытающегося с нами соединиться
                    byte[] bytes = new byte[handler.ReceiveBufferSize];
                    int bytesRec = handler.Receive(bytes);
                    //тут нужно узнавать версию формата из полученных данных, а не указывать из вне.
                    var message = service.Deserialize<TextMessage>(bytes);

                    // Показываем данные на консоли
                    Console.WriteLine("{0}: {1}{2}", message.TimeStamp, message.Text, Environment.NewLine);

                    // Отправляем ответ клиенту\
                    string reply = "Спасибо за запрос в " + message.Text.Length + " символов";
                    byte[] msg = Encoding.UTF8.GetBytes(reply);
                    handler.Send(msg);

                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
        }
    }
}
